console.log("Hello World");

let nome = "Guilherme";
let idade = 19;
let isProfessor = false;

console.log(typeof (nome));
console.log(typeof (idade));
console.log(typeof (isProfessor));

//concatenação
console.log("A soma de 2 + 2 é " + (2 + 2));

console.log(`A soma de 2 + 2 é ${2 + 2}`);

//console.log("Alguma coisa");
//alert("alguma coisa");

function escreverNoConsole() {
    console.log("Olá");
}

escreverNoConsole();

function podeDirigir(idade, cnh) {
    if (idade >= 18 && cnh == true) {
        console.log("Pode Dirigir");
    } else {
        console.log("Não Pode Dirigir");
    }
}

podeDirigir(19, false);

const parOuImpar = (valor) => {
    if (valor % 2 == 0) {
        console.log("É par");
        console.log(`O número ${valor} é Par`);
    } else {
        console.log("É ímpar");
        console.log(`O número ${valor} é Ímpar`);
    }
}

parOuImpar(99);

const parOuImpar2 = (valor) => {
    if (valor % 2 == 0) {
        return true;
    } else {
        return false;
    }
}

if (parOuImpar2(100)) {
    console.log("É par!");
} else {
    console.log("É ímpar!");
}

/*for(let x=1; x<=10; x++){
    console.log("O valor de x é " + x);
}*/

let numeros = [1,3,5,8,12];

console.log(numeros);

console.log(numeros.length);

console.log(numeros[0]);

let peixes = ['acara-bandeira','palhaço','mandarim','esturjão'];

console.log(peixes);
remover_ultimo = peixes.pop();
console.log("Peixe removido: " + remover_ultimo);

adicionar = peixes.push('Peixe Espada');

console.log(peixes);